package labs.springboot;

import org.springframework.boot.SpringApplication;

/**
 * This Launcher for the spring boot application.
 * 
 *
 */
public class SpringBootApplicationLauncher {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldController.class, args);
	}


}